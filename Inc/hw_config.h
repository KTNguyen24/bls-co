/*!
 * hw_config.h
 *
 * Handles information on what sensors the following board contains.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#ifndef _HW_CONFIG_H_
#define _HW_CONFIG_H_

#include <stdbool.h>
#include <stdint.h>

typedef enum
{
  HW_CONFIG_0024A00,               /* Circular LoRa Beacon */
  HW_CONFIG_000AA00,               /* Large LoRa Beacon */
  HW_CONFIG_0004A00,               /* BLS Com Board */
} HW_CONFIG_MAIN_BOARD_TYPE;

typedef enum
{
  HW_CONFIG_NO_SECOND_BOARD,
  HW_CONFIG_0005C03,            /* Lsm6d/4-pin connector */
  HW_CONFIG_0005C05,            /* GPS/Lsm6d/4-pin connector */
} HW_CONFIG_SECOND_BOARD_TYPE;

typedef enum
{
  HW_CONFIG_NO_THIRD_BOARD,
  HW_CONFIG_0021B00,     /* Button/LED */
  HW_CONFIG_0023B00,     /* Button/LED/Hall Effect (Mag) */
  HW_CONFIG_0026B00,     /* Button/LED/Optical Prox (Prox) */
  HW_CONFIG_002BA00,     /* Hdc1080 Humidity/Temperature */
} HW_CONFIG_THIRD_BOARD_TYPE;

typedef enum
{
  HW_CONFIG_LORA_BEACON,
  HW_CONFIG_LARGE_LORA_BEACON,
  HW_CONFIG_BLS_BUTTON_ONLY,
  HW_CONFIG_BLS_MAG,
  HW_CONFIG_BLS_PROX,
  HW_CONFIG_BLS_GPS,
  HW_CONFIG_LARGE_LORA_BEACON_HUMIDITY,
  NUMBER_OF_HW_CONFIG_BUNDLES
} HW_CONFIG_BUNDLES;

typedef struct
{
  HW_CONFIG_MAIN_BOARD_TYPE main_board;
  HW_CONFIG_SECOND_BOARD_TYPE second_board;
  HW_CONFIG_THIRD_BOARD_TYPE third_board;
} hw_config_types;

typedef struct
{
  hw_config_types board_type;
  bool a2235h_exists;         /* GPS module */
  bool a3214e_exists;         /* hall effect sensor */
  bool ccs811b_exists;        /* VOC gas monitor */
  bool cw1280_exists;         /* small EEPROM */
  bool hdc1080_exists;        /* temperature/humidity sensor */
  bool lis2de_exists;         /* 3-axis IMU */
  bool lsm6dso_exists;        /* 6-axis IMU acc/gryo */
  bool mx25r64_exists;        /* large flast memory */
  bool rdn1_exists;           /* Resistor divider for battery brings voltage down by 3 */
  bool tmp235_exists;         /* on-board temperature sensor */
  bool vcnl36687s_exists;     /* optical proximity sensor */
  bool veble103_exists;       /* Viaanix BLE module */
} hw_config_data_type;

/*!
 * @brief  Based on define, updates the what hardware exists on the board.
 *
 * @param  None
 * @retval None
 *
 */
void HwConfig_Init(void);

/*!
 * @brief  Returns the current hardware configuration.
 *
 * @param  None
 * @retval hw_config_data_type
 *
 */
hw_config_data_type HwConfig_GetData(void);

#endif
