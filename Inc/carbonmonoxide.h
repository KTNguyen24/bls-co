/*!
 *  carbonmonoxide.h
 *
 *  Handles collecting data from carbon monoxide sensor
 *  Created on: Nov 7, 2019
 *  Author: Viaanix
 *
 */

#ifndef _CARBONMONOXIDE_H_
#define _CARNONMONOXIDE_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
  uint16_t adc;
} carbonmonoxide_data_type;

/*!
 * @brief  Returns carbon monoxide related data.
 *
 * @param  con - carbonmonoxide_data_type pointer to contain the data.
 * @retval bool
 *         true - Data is valid
 *         false - Data is invalid
 *
 */
bool Carbonmonoxide_GetData(carbonmonoxide_data_type* con);

#endif
