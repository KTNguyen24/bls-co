/*!
 *  temperature.h
 *
 *  Handles collecting and calculating battery voltage from device.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#ifndef _TEMPERATURE_H_
#define _TEMPERATURE_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
  uint16_t adc;
} temperature_data_type;

/*!
 * @brief  Returns temperature related data.
 *
 * @param  con - temperature_data_type pointer to contain the data.
 * @retval bool
 *         true - Data is valid
 *         false - Data is invalid
 *
 */
bool Temperature_GetData(temperature_data_type* con);

#endif
