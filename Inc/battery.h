/*!
 *  battery.h
 *
 *  Handles collecting and calculating battery voltage from device.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#ifndef _BATTERY_H_
#define _BATTERY_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
  uint16_t adc;
} battery_data_type;

/*!
 * @brief  Returns battery related data.
 *
 * @param  con - battery_data_type container for battery data.
 * @retval bool
 *         true - If battery data is valid
 *         false - If battery data is invalid.
 *
 */
bool Battery_GetData(battery_data_type* con);

#endif
