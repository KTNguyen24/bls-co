/*
 * sleep.h
 *
 *  Created on: Aug 4, 2019
 *  Author: Viaanix
 *
 */

#ifndef _SLEEP_H_
#define _SLEEP_H_

#include <stdbool.h>
#include <stdint.h>

/*!
 * @brief  Puts the device in an initial known wake state.
 *
 * @param  None
 * @retval None
 *
 */
void Sleep_Init(void);

/*!
 * @brief  Handles the state machine for putting the device to sleep.
 *         Makes sure no main processes are not running then puts to sleep.
 *         Also, handles waking up the device.
 *
 * @param  None
 * @retval None
 *
 */
void Sleep_Process(void);

#endif
