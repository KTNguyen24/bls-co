/*
 * acc.h
 *
 * Handles high level state machine for accelerometer logic.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#ifndef _ACC_H_
#define _ACC_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
  uint16_t mg_x;
  uint16_t mg_y;
  uint16_t mg_z;
} acc_data_type;

typedef struct
{
  bool enabled;
  uint16_t check_in_time_minutes;
} acc_settings_type;

/*!
 * @brief  Initializes on-board accelerometer and puts in stable state.
 *
 * @param  settings - acc_settings_type ptr that contains initial settings.
 * @retval bool
 *         true - Successfully initialized accelerometer.
 *         false - Something failed during initialization of accelerometer.
 *
 */
bool Acc_Init(acc_settings_type* settings);

/*!
 * @brief  Handles the Accelerometer state machine.
 *
 * @param  None
 * @retval None
 *
 */
void Acc_Process(void);

/*!
 * @brief  Updates the accelerometer settings.
 *
 * @param  settings - acc_settings_type ptr that contains initial settings.
 * @retval bool
 *         true - Successfully initialized accelerometer.
 *         false - Something failed during initialization of accelerometer.
 *
 */
bool Acc_UpdateSettings(acc_settings_type* settings);

#endif
