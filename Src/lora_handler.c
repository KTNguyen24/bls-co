/*!
 * lora_handler.c
 *
 *  Created on: Aug 4, 2019
 *  Author: Viaanix
 *
 */

#include "vx_os.h"
#include "bsp.h"
#include "datastore.h"
#include "lora.h"
#include "lora_handler.h"
#include "hw.h"
#include "timeServer.h"
#include "vcom.h"
#include "version.h"

#define LORAWAN_ADR_STATE                       LORAWAN_ADR_OFF
#define LORAWAN_DEFAULT_DATA_RATE               DR_1
#define LORAWAN_APP_PORT                        2
#define LORAWAN_DEFAULT_CLASS                   CLASS_A
#define LORAWAN_DEFAULT_CONFIRM_MSG_STATE       LORAWAN_UNCONFIRMED_MSG
#define LORAWAN_APP_DATA_BUFF_SIZE              255
#define LORAWAN_DEFAULT_RX_TIME_MS              (10000)
#define LORAWAN_DEFAULT_CONFIRMED_RX_TIME_MS    (20000)
#define LORAWAN_DEFAULT_JOIN_TIME_MS            (60000)
#define LORAWAN_MAX_MISSED_CONFIRMS             (10)
#define LORAWAN_MAX_JOINS_BEFORE_RESET          (5)

typedef struct
{
  bool process;
  uint8_t buff[LORAWAN_APP_DATA_BUFF_SIZE];
  uint8_t size;
} lora_rx_response_type;

static bool LoraHandlerInit = false;
static uint8_t AppDataBuff[LORAWAN_APP_DATA_BUFF_SIZE];
static TimerEvent_t LoraHandlerRxTimer;
static lora_rx_response_type LoraHandlerVxOsResponse = { .process=false, .size=0 };
static LoraFlagStatus LoraHandlerMacProcessRequest = LORA_RESET;
static bool LoraHandlerBusy = false;
static bool LoraHandlerWaitingOnResponse = false;
static bool LoraHandlerRequestJoin = true;
static uint8_t LoraHandlerConfirmedRxMissed = 0;
static uint8_t LoraHandlerJoinCounter = 0;
static lora_AppData_t AppData =
{
  AppDataBuff,
  0,
  0
};

static  LoRaParam_t LoRaParamInit =
{
  LORAWAN_ADR_STATE,
  LORAWAN_DEFAULT_DATA_RATE,
  LORAWAN_PUBLIC_NETWORK
};

static void loraHandlerProcessMacNotify(void);
static void loraHandlerHasJoined(void);
static void loraHandlerProcessRxAppData(lora_AppData_t *app_data);
static void loraHandlerConfirmClass(DeviceClass_t class);
static void loraHandlerTxNeeded(void);
static void loraHandlerTxRxPacket(void);
static void loraHandlerRxTimeout(void* context);
static LoRaMainCallback_t LoRaMainCallbacks =
{
  HW_GetBatteryLevel,
  HW_GetTemperatureLevel,
  HW_GetCarbonmonoxideLevel,
  HW_GetUniqueId,
  HW_GetRandomSeed,
  loraHandlerProcessRxAppData,
  loraHandlerHasJoined,
  loraHandlerConfirmClass,
  loraHandlerTxNeeded,
  loraHandlerProcessMacNotify
};

/*=== LoraHandler_Init ===*/
void LoraHandler_Init(void)
{
  if(LoraHandlerInit == false)
  {
    datastore_settings_type ds;
    Datastore_GetSettings(&ds);
    LORA_SetCommissioning((const char*)ds.dev_eui, (const char*)ds.app_eui, (const char*)ds.app_key);
    LORA_Init(&LoRaMainCallbacks, &LoRaParamInit);
    LORA_Join();
    TimerInit( &LoraHandlerRxTimer, loraHandlerRxTimeout);
    TimerSetValue(&LoraHandlerRxTimer,  LORAWAN_DEFAULT_JOIN_TIME_MS);
    LoraHandlerInit = true;
  }
}

/*=== LoraHandler_Process ===*/
void LoraHandler_Process(void)
{
  if(LoraHandlerMacProcessRequest == LORA_SET)
  {
    /*reset notification flag*/
    LoraHandlerMacProcessRequest = LORA_RESET;
    LoRaMacProcess();
  }

  loraHandlerTxRxPacket();
}

/*=== LoraHandler_IsBusy ===*/
bool LoraHandler_IsBusy(void)
{
  if(LoraHandlerInit == false)
  {
    return false;
  }

  if(LoraHandlerMacProcessRequest == LORA_SET)
  {
    return true;
  }

  if(LoraHandlerBusy == true)
  {
    return true;
  }

  if(LoraHandlerRequestJoin == true)
  {
    return true;
  }

  return false;
}

/*!
 * @brief  Handles a notification from LoRa MAC
 *
 * @param  None
 * @retval None
 *
 */
static void loraHandlerProcessMacNotify(void)
{
  LoraHandlerMacProcessRequest = LORA_SET;
}

/*!
 * @brief  Notifies handler that LoRa Radio has joined network.
 *
 * @param  None
 * @retval None
 *
 */
static void loraHandlerHasJoined(void)
{
  TimerStop(&LoraHandlerRxTimer);
#if( OVER_THE_AIR_ACTIVATION != 0 )
  PRINTF("JOINED\n\r");
#endif
  LoraHandlerJoinCounter = 0;
  VxOs_StartProcess();
  LoraHandlerBusy = false;
  //LORA_RequestClass(LORAWAN_DEFAULT_CLASS);
}

/*!
 * @brief  Callback when application data is received from LoRa MAC.
 *
 * @param  app_data - lora_AppData_t pointer
 */
static void loraHandlerProcessRxAppData(lora_AppData_t *app_data)
{
  PRINTF("PACKET RECEIVED ON PORT %d\n\r", app_data->Port);
  LoraHandlerConfirmedRxMissed = 0;
  switch(app_data->Port)
  {
    case VX_OS_LORAWAN_PORT:
    {
      TimerStop(&LoraHandlerRxTimer);
      LoraHandlerVxOsResponse.process = true;
      memcpy(LoraHandlerVxOsResponse.buff, app_data->Buff, app_data->BuffSize);
      LoraHandlerVxOsResponse.size = app_data->BuffSize;
      break;
    }

    default:
    {
      break;
    }
  }
}

/*!
 * @brief  Informs network server confirming class.
 *
 * @param  class - DeviceClass_t containing information about class from server.
 * @retval None
 *
 */
static void loraHandlerConfirmClass(DeviceClass_t class)
{
  PRINTF("switch to class %c done\n\r","ABC"[class] );

  /*Optionnal*/
  /*informs the server that switch has occurred ASAP*/
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG);
}

/*!
 * @brief  Sends message back when network server sent confirmed message
 *
 * @param  None
 * @retval None
 *
 */
static void loraHandlerTxNeeded(void)
{
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send(&AppData, LORAWAN_UNCONFIRMED_MSG);
}

/*!
 * @brief  Checks if there is anything in the queues to be sent or if we are waiting on an RX.
 *
 * @param  None
 * @retval None
 *
 */
static void loraHandlerTxRxPacket(void)
{
  if ( LORA_JoinStatus () != LORA_SET)
  {
    if(LoraHandlerRequestJoin == true)
    {
      LoraHandlerRequestJoin = false;
      LoraHandlerJoinCounter++;
      if(LoraHandlerJoinCounter >= LORAWAN_MAX_JOINS_BEFORE_RESET)
      {
        NVIC_SystemReset();
      }
      LORA_Join();
    }
    return;
  }

  if(LoraHandlerWaitingOnResponse == true)
  {
    if(LoraHandlerVxOsResponse.process == true)
    {
      LoraHandlerWaitingOnResponse = false;
      LoraHandlerVxOsResponse.process = false;
      VxOs_HandleRx(LoraHandlerVxOsResponse.buff, LoraHandlerVxOsResponse.size);
    }
  }
  else
  {
    vx_os_data_type data;
    if(VxOs_IsDataToSend(&data) == true)
    {
      AppData.Port = VX_OS_LORAWAN_PORT;
      AppData.BuffSize = 0;
      AppData.Buff[AppData.BuffSize++] = 0; // VX-Link LoRa header, DT Size 1 Byte, OS Check-in, no additional header bytes
      AppData.Buff[AppData.BuffSize++] = 0x1A; // OS Check-in Type 1 Data Type Identifier
      AppData.Buff[AppData.BuffSize++] = (data.battery_adc&0xFF00)>>8;
      AppData.Buff[AppData.BuffSize++] = (data.battery_adc&0x00FF);
      AppData.Buff[AppData.BuffSize++] = (data.temperature_adc&0xFF00)>>8;
      AppData.Buff[AppData.BuffSize++] = (data.temperature_adc&0x00FF);
      AppData.Buff[AppData.BuffSize++] = (data.carbonmonoxide_adc&0xFF00)>>8;
      AppData.Buff[AppData.BuffSize++] = (data.carbonmonoxide_adc&0x00FF);
      AppData.Buff[AppData.BuffSize++] = (data.firmware_version&0xFF000000)>>24;
      AppData.Buff[AppData.BuffSize++] = (data.firmware_version&0x00FF0000)>>16;
      AppData.Buff[AppData.BuffSize++] = (data.firmware_version&0x0000FF00)>>8;
      AppData.Buff[AppData.BuffSize++] = (data.firmware_version&0x000000FF);
      AppData.Buff[AppData.BuffSize++] = (data.flags);
      if(LORA_JoinStatus() == LORA_SET)
      {
        LORA_send(&AppData, LORAWAN_CONFIRMED_MSG);
        LoraHandlerConfirmedRxMissed++;
        if(LoraHandlerConfirmedRxMissed >= LORAWAN_MAX_MISSED_CONFIRMS)
        {
          //NVIC_SystemReset();
        }
        TimerSetValue(&LoraHandlerRxTimer,  LORAWAN_DEFAULT_CONFIRMED_RX_TIME_MS);
        TimerStart(&LoraHandlerRxTimer);
        LoraHandlerBusy = true;
        LoraHandlerWaitingOnResponse = true;
      }
    } // for another application, need to put else if for its fifo's
    else
    {
      LoraHandlerBusy = false;
    }
  }
}

/*!
 * @brief  Timer callback if response was not received from LoRaWAN.
 *
 * @param  None
 * @retval None
 *
 */
static void loraHandlerRxTimeout(void* context)
{
  if(LORA_JoinStatus() != LORA_SET)
  {
    LoraHandlerRequestJoin = true;
  }
  else
  {
    TimerStop(&LoraHandlerRxTimer);
    LoraHandlerWaitingOnResponse = false;
    LoraHandlerVxOsResponse.process = false;
  }
}
