/*!***************************************************************************
  * @file    led.c
  * @author  Viaanix
  * @version V1.0.0
  * @date    21-April-2019
  * @brief   handles states of LEDs.
  ****************************************************************************
  */
  
#include "led.h"
#include "hw.h"
#include "hw_config.h"

#define VX_24A_LED_GPIO_PORT     (GPIOB)
#define VX_24A_LED_PIN           (GPIO_PIN_0)
#define VX_24A_LED_GPIO_CLOCK    __HAL_RCC_GPIOB_CLK_ENABLE

typedef struct
{
  bool state;
  GPIO_TypeDef* port;
  uint16_t pin;
} led_data_t;

static bool LedInit = false;
static led_data_t Led[NUMBER_OF_LEDS];

static void ledUpdate(LED_TYPE index);

// Led_Init
void Led_Init(void)
{
  if(LedInit == false)
  {
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    Led[STATUS_LED].state = false;
    
    Led[STATUS_LED].pin = VX_24A_LED_PIN;
    Led[STATUS_LED].port = VX_24A_LED_GPIO_PORT;
    VX_24A_LED_GPIO_CLOCK();
    GPIO_InitStruct.Pin =  VX_24A_LED_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(VX_24A_LED_GPIO_PORT, &GPIO_InitStruct);

    LedInit = true;
  }
}

/*=== Led_DeInit ===*/
void Led_DeInit(void)
{
  if(LedInit == true)
  {
    uint8_t i = 0;
    for(i=0; i<NUMBER_OF_LEDS; i++)
    {
      Led_Off(i);
    }
    LedInit = false;
  }
}

/*=== Led_On ===*/
bool Led_On(LED_TYPE index)
{
  if(LedInit == false)
  {
    return false;
  }
  
  Led[index].state = true;
  ledUpdate(index);
  return true;
}

/*=== Led_Off ===*/
bool Led_Off(LED_TYPE index)
{
  if(LedInit == false)
  {
    return false;
  }
  
  Led[index].state = false;
  ledUpdate(index);
  return true;
}

/*=== Led_Toggle ===*/
bool Led_Toggle(LED_TYPE index)
{
  if(LedInit == false)
  {
    return false;
  }
  
  if(Led[index].state == true)
  {
    Led_Off(index);
  }
  else
  {
    Led_On(index);
  }
  return true;
}

/*!
 * @brief  Updates the LED hardware based on state.
 * @param  [IN] index - LED_TYPE that contains the LED to update.
 *
 * @retval None
 *
 */
static void ledUpdate(LED_TYPE index)
{
  if(Led[index].state == true)
  {
    HAL_GPIO_WritePin(Led[index].port, Led[index].pin, GPIO_PIN_SET);
  }
  else
  {
    HAL_GPIO_WritePin(Led[index].port, Led[index].pin, GPIO_PIN_RESET);
  }
}
