/*
 * acc.c
 *
 * Handles high level state machine for accelerometer logic.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#include "acc.h"
#include "datastore.h"
#include "lsm6dso.h"

#define ACC_LORAWAN_PORT (4)

#define DEFAULT_LSM6DSO_ACC_MODE   (LSM6DSO_ACC_MODE_NORMAL)
#define DEFAULT_LSM6DSO_ACC_RANGE  (LSM6DSO_ACC_RANGE_2G)
#define DEFAULT_LSM6DSO_ACC_ODR    (LSM6DSO_ACC_ODR_POWER_DOWN)

#define ACC_CHECK_IN_DISABLED              (0)
#define ACC_CHECK_IN_TIME_MINUTES_MIN      (1)
#define ACC_CHECK_IN_TIME_MINUTES_MAX      (1440)

typedef enum
{
  ACC_NO_ERROR,
  ACC_ERROR_SETTINGS_DID_NOT_UPDATE,
} ACC_ERROR_TYPE;

typedef enum
{
  ACC_STATE_WAIT,
  ACC_STATE_GET_ACC_SNAPSHOT,
  ACC_STATE_QUEUE_DATA,
  ACC_STATE_ERROR
} ACC_STATE_TYPE;

static bool AccInit = false;
static ACC_STATE_TYPE AccState = ACC_STATE_WAIT;
static acc_settings_type AccSettings;

static bool accValidateSettings(acc_settings_type* settings);
static bool accUpdateSettings(acc_settings_type* settings);

/*=== Acc_Init ===*/
bool Acc_Init(acc_settings_type* settings)
{
  if(AccInit == false)
  {
    AccInit = true;
    if(Acc_UpdateSettings(settings) == false)
    {
      AccInit = false;
      return false;
    }
  }

  return true;
}

/*=== Acc_Process ===*/
void Acc_Process(void)
{
  if(AccInit == true)
  {
    switch(AccState)
    {
      default:
      {
        break;
      }
    }
  }
}

/*=== Acc_UpdateSettings ===*/
bool Acc_UpdateSettings(acc_settings_type* settings)
{
  if(AccInit == false)
  {
    return false;
  }

  if(accValidateSettings(settings) == false)
  {
    return false;
  }

  if(accUpdateSettings(settings) == false)
  {
    return false;
  }

  AccSettings = *settings;
  return true;
}

/*!
 * @brief  Validates the Acc settings.
 *
 * @param  [IN] settings - acc_settings_type ptr containing the new settings.
 * @retval bool
 *         true - New acc settings are valid
 *         false - At least one new acc setting is invalid
 *
 */
static bool accValidateSettings(acc_settings_type* settings)
{
  if(settings->enabled != false && settings->enabled != true)
  {
    return false;
  }

  return true;
}

/*!
 * @brief  Does the actual updating of the settings.
 *
 * @param  settings - acc_settings_type ptr containing settings to change to.
 * @retval bool
 *         true - Settings updated successfully.
 *         false - At least one setting did not update correctly.
 *
 */
static bool accUpdateSettings(acc_settings_type* settings)
{
  lsm6dso_settings_type hw_settings;
  if(Lsm6dso_GetSettings(&hw_settings) == false)
  {
    hw_settings.acc_mode = DEFAULT_LSM6DSO_ACC_MODE;
    hw_settings.acc_odr = DEFAULT_LSM6DSO_ACC_ODR;
    hw_settings.acc_range = DEFAULT_LSM6DSO_ACC_RANGE;
  }

  if(settings->enabled == true)
  {
    hw_settings.acc_mode = LSM6DSO_ACC_MODE_NORMAL;
    hw_settings.acc_range = LSM6DSO_ACC_RANGE_2G;
    hw_settings.acc_odr = LSM6DSO_ACC_ODR_12_5HZ;
  }
  else
  {
    hw_settings.acc_odr = LSM6DSO_ACC_ODR_POWER_DOWN;
  }

  if(Lsm6dso_UpdateSettings(&hw_settings) == false)
  {
    return false;
  }

  return true;
}
