/*
 *  battery.c
 * Hi battery two
 *  Handles collecting and calculating battery voltage from device.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#include "hw_msp.h"
#include "battery.h"

/*=== Battery_GetData ===*/
bool Battery_GetData(battery_data_type* con)
{
  con->adc = HW_GetBatteryAdc();
  return true;
}


