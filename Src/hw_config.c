/*!
 * hw_config.c
 *
 * Handles information on what sensors the following board contains.
 * Created on: Aug 14, 2019
 * Author: Viaanix
 *
 */

#include "hw_config.h"

/*
  HW_CONFIG_LORA_BEACON,
  HW_CONFIG_LARGE_LORA_BEACON,
  HW_CONFIG_BLS_BUTTON_ONLY,
  HW_CONFIG_BLS_MAG,
  HW_CONFIG_BLS_PROX,
  HW_CONFIG_BLS_GPS,
  HW_CONFIG_LARGE_LORA_BEACON_HUMIDITY,
*/
#define DEFAULT_HW_CONFIG_BUNDLE (HW_CONFIG_LORA_BEACON)

static hw_config_types HwConfigs[NUMBER_OF_HW_CONFIG_BUNDLES] =
{
  { .main_board=HW_CONFIG_0024A00, .second_board=HW_CONFIG_NO_SECOND_BOARD, .third_board=HW_CONFIG_NO_THIRD_BOARD },
  { .main_board=HW_CONFIG_000AA00, .second_board=HW_CONFIG_NO_SECOND_BOARD, .third_board=HW_CONFIG_NO_THIRD_BOARD },
  { .main_board=HW_CONFIG_0004A00, .second_board=HW_CONFIG_NO_SECOND_BOARD, .third_board=HW_CONFIG_0021B00 },
  { .main_board=HW_CONFIG_0004A00, .second_board=HW_CONFIG_0005C03,         .third_board=HW_CONFIG_0023B00 },
  { .main_board=HW_CONFIG_0004A00, .second_board=HW_CONFIG_0005C03,         .third_board=HW_CONFIG_0026B00 },
  { .main_board=HW_CONFIG_0004A00, .second_board=HW_CONFIG_0005C05,         .third_board=HW_CONFIG_0021B00 },
  { .main_board=HW_CONFIG_000AA00, .second_board=HW_CONFIG_NO_SECOND_BOARD, .third_board=HW_CONFIG_002BA00 },
};

static hw_config_data_type HwConfigData = {0};

static void hwConfigSetupMainBoardConfig(void);
static void hwConfigSetupSecondBoardConfig(void);
static void hwConfigSetupThirdBoardConfig(void);
static void hwConfigReset(void);

/*=== HwConfig_Init ===*/
void HwConfig_Init(void)
{
  HwConfigData.board_type = HwConfigs[DEFAULT_HW_CONFIG_BUNDLE];
  hwConfigReset();
  hwConfigSetupMainBoardConfig();
  hwConfigSetupSecondBoardConfig();
  hwConfigSetupThirdBoardConfig();
}

/*=== HwConfig_GetData ===*/
hw_config_data_type HwConfig_GetData(void)
{
  return HwConfigData;
}

/*!
 * @brief  Setups the main board configuration based on board selected.
 *
 * @param  None
 * @retval None
 *
 */
static void hwConfigSetupMainBoardConfig(void)
{
  switch(HwConfigData.board_type.main_board)
  {
    /* LoRa Beacon */
    case HW_CONFIG_0024A00:
    {
      HwConfigData.lsm6dso_exists = true;
      HwConfigData.tmp235_exists = true;
      break;
    }

    /* Large LoRa Beacon */
    case HW_CONFIG_000AA00:
    {
      HwConfigData.lsm6dso_exists = true;
      HwConfigData.tmp235_exists = true;
      break;
    }

    /* BLS */
    case HW_CONFIG_0004A00:
    {
      HwConfigData.veble103_exists = true;
      HwConfigData.rdn1_exists = true;
      HwConfigData.mx25r64_exists = true;
      HwConfigData.lis2de_exists = true;
      HwConfigData.tmp235_exists = true;
      break;
    }
  }
}

/*!
 * @brief  Setups the secondary board configuration based on board selected.
 *
 * @param  None
 * @retval None
 *
 */
static void hwConfigSetupSecondBoardConfig(void)
{
  switch(HwConfigData.board_type.second_board)
  {
    case HW_CONFIG_NO_SECOND_BOARD:
    {
      break;
    }

    /* LSM6DS0/4-pin connector */
    case HW_CONFIG_0005C03:
    {
      HwConfigData.cw1280_exists = true;
      HwConfigData.lsm6dso_exists = true;
      break;
    }

    /* LSM6DS0/GPS/4-pin connector */
    case HW_CONFIG_0005C05:
    {
      HwConfigData.a2235h_exists = true;
      HwConfigData.cw1280_exists = true;
      HwConfigData.lsm6dso_exists = true;
      break;
    }
  }
}

/*!
 * @brief  Setups the third board configuration based on board selected.
 *
 * @param  None
 * @retval None
 *
 */
static void hwConfigSetupThirdBoardConfig(void)
{
  switch(HwConfigData.board_type.third_board)
  {
    case HW_CONFIG_NO_THIRD_BOARD:
    {
      break;
    }

    /* Button/LED */
    case HW_CONFIG_0021B00:
    {
      break;
    }

    /* Button/LED/Hall Effect */
    case HW_CONFIG_0023B00:
    {
      HwConfigData.a3214e_exists = true;
      break;
    }

    /* Button/LED/Optical Prox */
    case HW_CONFIG_0026B00:
    {
      HwConfigData.vcnl36687s_exists = true;
      break;
    }

    /* Hdc1080 Humidity */
    case HW_CONFIG_002BA00:
    {
      HwConfigData.hdc1080_exists = true;
      break;
    }
  }
}

/*!
 * @brief  Reset Hardware config to put them all in a false state.
 *
 * @param  None
 * @retval None
 *
 */
static void hwConfigReset(void)
{
  HwConfigData.a2235h_exists = false;
  HwConfigData.a3214e_exists = false;
  HwConfigData.ccs811b_exists = false;
  HwConfigData.cw1280_exists = false;
  HwConfigData.hdc1080_exists = false;
  HwConfigData.lis2de_exists = false;
  HwConfigData.lsm6dso_exists = false;
  HwConfigData.mx25r64_exists = false;
  HwConfigData.rdn1_exists = false;
  HwConfigData.tmp235_exists = false;
  HwConfigData.vcnl36687s_exists = false;
  HwConfigData.veble103_exists = false;
}
