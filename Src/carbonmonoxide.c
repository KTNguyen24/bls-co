/*
 *  carbonmonoxide.c
 *
 *  Handles collecting data from carbon monoxide sensor
 *  Created on: Nov 7, 2019
 *  Author: Viaanix
 *
 */

#include "hw_msp.h"
#include "carbonmonoxide.h"

/*=== Carbonmonoxide_GetData ===*/
bool Carbonmonoxide_GetData(carbonmonoxide_data_type* con)
{
  con->adc = HW_GetCarbonmonoxideAdc();
  return true;
}
