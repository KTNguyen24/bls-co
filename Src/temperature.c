/*
 *  temperature.c
 *
 *  Handles collecting and calculating battery voltage from device.
 *  Created on: Aug 7, 2019
 *  Author: Viaanix
 *
 */

#include "hw_msp.h"
#include "temperature.h"

/*=== Temperature_GetData ===*/
bool Temperature_GetData(temperature_data_type* con)
{
  con->adc = HW_GetTemperatureAdc();
  return true;
}
