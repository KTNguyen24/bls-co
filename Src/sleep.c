/*
 * sleep.c
 *
 *  Created on: Aug 4, 2019
 *  Author: Viaanix
 *
 */

#include "hw.h"
#include "led.h"
#include "lora_handler.h"
#include "low_power_manager.h"
#include "sleep.h"
#include "vx_os.h"

static bool SleepInit = false;

/*=== Sleep_Init ===*/
void Sleep_Init(void)
{
  if(SleepInit == false)
  {
    SleepInit = true;
  }
}

/*=== Sleep_Process ===*/
void Sleep_Process(void)
{
  if(SleepInit == false)
  {
    return;
  }


  DISABLE_IRQ();
  if( (LoraHandler_IsBusy() == false) &&
      (VxOs_IsBusy() == false) )
  {
#ifndef LOW_POWER_DISABLE
    LPM_EnterLowPower();
#endif
  }
  ENABLE_IRQ();
  Led_On(STATUS_LED);
}
